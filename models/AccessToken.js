/**
 * Created by hjshin on 15. 10. 3..
 */
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var AccessTokenSchema = new Schema({
    user_id : String,
    token : String,
    create_date:  {type: Date, default:Date.now},
    update_date:  {type: Date, default:Date.now}
}, { versionKey: false });

AccessTokenSchema.index({"update_date":1}, {expireAfterSeconds:10});

module.exports = mongoose.model('AccessToken', AccessTokenSchema);