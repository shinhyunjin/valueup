/**
 * Created by hjshin on 15. 10. 3..
 */
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    id : String,
    pw : String,
    create_date:  {type: Date, default:new Date()}
}, { versionKey: false });

module.exports = mongoose.model('User', UserSchema);