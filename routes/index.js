var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = require('../models/User');
var AccessToken = require('../models/AccessToken');

// mongo db connection
var mongodbcfg = {
  dsn : "mongodb://localhost:27017/valueup",
  options: {
    server : {
      auto_reconnect : true
    }
  }
};

mongoose.connect(mongodbcfg.dsn, mongodbcfg.options);

var db = mongoose.connection;

db.on('error', function(err) {
  console.error('MongoDB connection error:', err);
});

db.once('open', function callback() {
  console.info('MongoDB connection is established');
});

db.on('disconnected', function() {
  console.error('MongoDB disconnected!');
  mongoose.connect(mongodbcfg.dsn, mongodbcfg.options);
});

db.on('reconnected', function() {
  console.info('MongoDB reconnected!');
});

ObjectId = mongoose.Types.ObjectId;

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* POST signup */
router.post('/signup', function(req, res, next) {
  console.log("SIGNUP");
  console.log("req.body =>" + JSON.stringify(req.body));

  var _user = new User();
  _user.id = req.body.id;
  _user.pw = req.body.pw;

  _user.save(function(err){
    if(err) {
      res.status(500).send({code:500, message:err});
    } else {
      res.status(200).send({code:200, message:"signup ok!"});
    }
  });
});

/* POST login */
router.post('/login', function(req, res, next) {
  console.log("login");
  console.log("req.body =>" + JSON.stringify(req.body));


  User.findOne({id:req.body.id, pw:req.body.pw}, function(err, _user) {
    if(err) {
      res.status(500).send({code:500, message:err});
    } else if(_user) {
      //console.log("accesstoken =>" + AccessToken);
      //console.log("id => " + req.body.id);
      AccessToken.findOne({user_id:req.body.id}, function(err, token) {
        if(err) {
          res.status(500).send({code:500, message:err});
        } else if(token){
          AccessToken.update({token:req.body.token}, {update_date:new Date()}, function(err) {
            if(err) {
              res.status(500).send({code:500, message:"token update fail!!"});
            } else {
              res.status(200).send({code:200, data:_user, token:token.token});
            }
          });
        } else {
          var _access_token = new AccessToken();
          _access_token.user_id = req.body.id;
          _access_token.token = "TOKEN!!!!!!";
          _access_token.save(function(err) {
            if(err) {
              res.status(500).send({code:500, message:"fail to create token!"});
            } else {
              res.status(200).send({code:200, data:_user, token:_access_token.token});
            }
          });
        }
      });
    } else {
      res.status(400).send({code:400, message:"not found login credentials"});
    }
  });
});

/* POST test */
router.post('/test', function(req, res, next) {
  console.log("test");
  console.log("req.body =>" + JSON.stringify(req.body));

  AccessToken.findOne({token:req.body.token}, function(err, token) {
    if(err) {
      res.status(500).send({code:500, message:err});
    } else if(token){
      AccessToken.update({token:req.body.token}, {update_date:new Date()}, function(err) {
        if(err) {
          res.status(500).send({code:500, message:"token update fail!!"});
        } else {
          res.status(200).send({code:200, message:"token ok!!"});
        }
      });
    } else {
      res.status(400).send({code:400, message:"token isn't validate...."});
    }
  });
});

module.exports = router;
